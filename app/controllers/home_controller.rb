class HomeController < ApplicationController
  def index
    @pages = Page.last(3).reverse
  end

  def about
  end
end
